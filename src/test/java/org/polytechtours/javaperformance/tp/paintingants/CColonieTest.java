package org.polytechtours.javaperformance.tp.paintingants;
import java.util.Vector;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class CColonieTest {

    @org.junit.jupiter.api.Test
    void pleaseStop() {

        /* Initialisation */
        int colonie_size = 10;
        Vector<CFourmi> fourmis = new Vector<>(colonie_size);
        for (int i = 0; i < colonie_size; i++) {
            int red = (int) (Math.random() * 256);
            int green = (int) (Math.random() * 256);
            int blue = (int) (Math.random() * 256);
            Color couleurDeposee = new Color(red, green, blue);

            red = (int) (Math.random() * 256);
            green = (int) (Math.random() * 256);
            blue = (int) (Math.random() * 256);
            Color couleurSuivie = new Color(red, green, blue);

            float probaTD, probaG, probaD, probaSuivre;
            probaTD = (float) (Math.random());
            probaG = (float) (Math.random() * (1.0 - probaTD));
            probaD = (float) (1.0 - (probaTD + probaG));
            probaSuivre = (float) (0.5 + 0.5 * Math.random());

            PaintingAnts appli = new PaintingAnts();
            Dimension dimension = new Dimension(100, 100);
            char typeDeplacement = 'o';
            CPainting painting = new CPainting(dimension, appli);

            float init_x = (float) Math.random();
            float init_y = (float) Math.random();
            int initDirection = (int) (Math.random() * 8);
            int taille = (int) (Math.random() * 4);
            float seuilLuminance = 40f;

            CFourmi fourmi = new CFourmi(couleurDeposee, couleurSuivie, probaTD, probaG, probaD, probaSuivre, painting,
                    typeDeplacement, init_x, init_y, initDirection, taille, seuilLuminance, appli);
            fourmis.add(fourmi);
        }
        PaintingAnts appli = new PaintingAnts();
        ;

        CColonie colonie = new CColonie(fourmis, appli);

        /* Test */
        colonie.pleaseStop();

        /* Assertions */
        assertFalse(colonie.getmContinue(), "mContinue doit être égal à false");
    }
}

