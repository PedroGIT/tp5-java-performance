package org.polytechtours.javaperformance.tp.paintingants;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PaintingAntsTest {

    @Test
    void compteur() {
        /* Initialisation */
        PaintingAnts appli = new PaintingAnts();

        /* Test */
        long compteur_avant = appli.getmCompteur();
        appli.compteur();
        long compteur_apres = appli.getmCompteur();

        /* Assertions */
        assertEquals(compteur_avant + 1, compteur_apres, "le compteur doit être égal à compteur avant + 1");
    }

    @Test
    void destroy() {
        /* Initialisation */
        PaintingAnts appli = new PaintingAnts();

        /* Test */
        appli.destroy();

        /* Assertions */
        assertNull(appli.getmApplis(), "l'objet doit être Null après le destroy()");
    }

    @Test
    void getAppletInfo() {
        /* Initialisation */
        PaintingAnts appli = new PaintingAnts();

        /* Assertions */
        assertEquals("Painting Ants", appli.getAppletInfo());
    }

    @Test
    void incrementFpsCounter() {
        /* Initialisation */
        PaintingAnts appli = new PaintingAnts();

        /* Test */
        Long fps_avant = appli.getFpsCounter();
        appli.IncrementFpsCounter();
        Long fps_apres = appli.getFpsCounter();

        /* Assertions */
        assertEquals(fps_avant + 1, fps_apres, "fps apres doit etre égal à fps avant + 1");
    }

    @Test
    void pause() {
        /* Initialisation */
        PaintingAnts appli = new PaintingAnts();

        /* Test */
        boolean pause_avant = appli.getPause();
        appli.pause();
        boolean pause_apres = appli.getPause();
        appli.pause();
        boolean pause_deux_fois = appli.getPause();

        /* Assertions */
        assertNotEquals(pause_avant, pause_apres);
        assertEquals(pause_avant,pause_deux_fois);
    }
}