package paintingants;

import static org.junit.Assert.assertNotNull;

import java.awt.Color;
import java.awt.Dimension;

import org.junit.jupiter.api.Test;
import org.polytechtours.javaperformance.tp.paintingants.CFourmi;
import org.polytechtours.javaperformance.tp.paintingants.CPainting;
import org.polytechtours.javaperformance.tp.paintingants.PaintingAnts;

class CFourmiTest {

	@Test
	void getNbDeplacementsTest() {
		/* Initialisation */
		int red = (int) (Math.random() * 256);
		int green = (int) (Math.random() * 256); 
		int blue = (int) (Math.random() * 256);
		Color couleurDeposee = new Color(red, green, blue);
		
		red = (int) (Math.random() * 256); 
		green = (int) (Math.random() * 256); 
		blue = (int) (Math.random() * 256);
		Color couleurSuivie = new Color(red, green, blue);
		
		float probaTD, probaG, probaD, probaSuivre;
		probaTD = (float) (Math.random());
        probaG = (float) (Math.random() * (1.0 - probaTD));
        probaD = (float) (1.0 - (probaTD + probaG));
        probaSuivre = (float) (0.5 + 0.5 * Math.random());
        
        PaintingAnts appli = new PaintingAnts();
        Dimension dimension = new Dimension(100,100);
        char typeDeplacement = 'o';
        CPainting painting = new CPainting(dimension, appli);
        
        float init_x = (float) Math.random();
        float init_y = (float) Math.random();
        int initDirection = (int) (Math.random() * 8);
        int taille = (int) (Math.random() * 4);
        float seuilLuminance = 40f;
        
        CFourmi fourmi = new CFourmi(couleurDeposee, couleurSuivie, probaTD, probaG, probaD, probaSuivre, painting,
                typeDeplacement, init_x, init_y, initDirection, taille, seuilLuminance, appli);
        
        /* Test */
        long nbDeplacement = fourmi.getNbDeplacements();
        
        /* Assertions */
        assertNotNull("nbDeplacement ne doit pas etre null", nbDeplacement);
	}
	
	@Test
	void getXTest() {
		/* Initialisation */
		int red = (int) (Math.random() * 256);
		int green = (int) (Math.random() * 256); 
		int blue = (int) (Math.random() * 256);
		Color couleurDeposee = new Color(red, green, blue);
		
		red = (int) (Math.random() * 256); 
		green = (int) (Math.random() * 256); 
		blue = (int) (Math.random() * 256);
		Color couleurSuivie = new Color(red, green, blue);
		
		float probaTD, probaG, probaD, probaSuivre;
		probaTD = (float) (Math.random());
        probaG = (float) (Math.random() * (1.0 - probaTD));
        probaD = (float) (1.0 - (probaTD + probaG));
        probaSuivre = (float) (0.5 + 0.5 * Math.random());
        
        PaintingAnts appli = new PaintingAnts();
        Dimension dimension = new Dimension(100,100);
        char typeDeplacement = 'o';
        CPainting painting = new CPainting(dimension, appli);
        
        float init_x = (float) Math.random();
        float init_y = (float) Math.random();
        int initDirection = (int) (Math.random() * 8);
        int taille = (int) (Math.random() * 4);
        float seuilLuminance = 40f;
        
        CFourmi fourmi = new CFourmi(couleurDeposee, couleurSuivie, probaTD, probaG, probaD, probaSuivre, painting,
                typeDeplacement, init_x, init_y, initDirection, taille, seuilLuminance, appli);
        
        /* Test */
        int x = fourmi.getX();
        
        /* Assertions */
        assertNotNull("x ne doit pas etre null", x);
	}
	
	@Test
	void getYTest() {
		/* Initialisation */
		int red = (int) (Math.random() * 256);
		int green = (int) (Math.random() * 256); 
		int blue = (int) (Math.random() * 256);
		Color couleurDeposee = new Color(red, green, blue);
		
		red = (int) (Math.random() * 256); 
		green = (int) (Math.random() * 256); 
		blue = (int) (Math.random() * 256);
		Color couleurSuivie = new Color(red, green, blue);
		
		float probaTD, probaG, probaD, probaSuivre;
		probaTD = (float) (Math.random());
        probaG = (float) (Math.random() * (1.0 - probaTD));
        probaD = (float) (1.0 - (probaTD + probaG));
        probaSuivre = (float) (0.5 + 0.5 * Math.random());
        
        PaintingAnts appli = new PaintingAnts();
        Dimension dimension = new Dimension(100,100);
        char typeDeplacement = 'o';
        CPainting painting = new CPainting(dimension, appli);
        
        float init_x = (float) Math.random();
        float init_y = (float) Math.random();
        int initDirection = (int) (Math.random() * 8);
        int taille = (int) (Math.random() * 4);
        float seuilLuminance = 40f;
        
        CFourmi fourmi = new CFourmi(couleurDeposee, couleurSuivie, probaTD, probaG, probaD, probaSuivre, painting,
                typeDeplacement, init_x, init_y, initDirection, taille, seuilLuminance, appli);
        
        /* Test */
        int y = fourmi.getY();
        
        /* Assertions */
        assertNotNull("y ne doit pas etre null", y);
	}

}
